
default: charte.pdf fede.pdf regle.pdf

diff: charte.diff.pdf fede.diff.pdf regle.diff.pdf

%.pdf: %.tex
	pdflatex $*
	pdflatex $*

%.diff.tex: %.tex
	@if [ -z "$${LAST}" ]; then echo "Il faut passer une variable LAST=<tag> à la commande"; exit 1; fi
	@git show ${LAST}:$< > tmp.tex
	@if grep -F -q 'usepackage[latin1]' tmp.tex; then \
		iconv -f latin1 -t utf-8 tmp.tex > tmp2.tex; \
		sed -e 's/usepackage[latin1]/usepackage[utf8]/' tmp2.tex > tmp.tex; \
		rm tmp2.tex; \
	fi
	latexdiff tmp.tex $< > $@
	@rm -f tmp.tex
